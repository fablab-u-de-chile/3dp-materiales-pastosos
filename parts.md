# Partes y piezas


## Partes comerciales

 ITEM              | Cantidad
 ---------------------------   | ------------
 Tuerca Tr8*8 bronce | 1
 Husillo trapezoidal Tr8 | 210 mm
 Eje de acero plata 8 mm | 450 mm
 Varilla roscada M8 | 650 mm
 Tuerca M8 | 11
 Golilla M8 | 8
 Jeringa 60cc | 2
 Tubo aluminio 9.6 mm | 150 mm
 Rodamiento LM08UU | 2
 Acople flexible 5-8 mm | 1
 Acople rápido manguera 4 mm |
 Manguera de 6 mm |
 Manguera de 4 mm | 
 Adaptador manguera 4-6 mm |
 Tuerca M6 | 1
 Perno M3x20 | 9
 Perno M3x15 | 10
 Perno M3x10 | 4
 Tuerca M3 | 21

## Partes impresas

Los archivos .step y .stl los puedes encontrar en la [carpeta de partes](parts/). Las boquillas intercambiables de imprimieron en SLA para mayor precisión.

IMPORTANTE: La componente PortaJeringa60cc es la que se adosa a la impresora 3D a utilizar. Esta pieza es compatible exclusivamente con el cabezal de una impresora Makerbot Replicator 2, por lo que para usar este sistema en otra impresora será necesario hacer modificaciones en esta pieza o diseñar una pieza extra. También puede que sea necesario hacer piezas extra para el contacto apropiado con los sensores de fin de carrera.

 ITEM                   | Cantidad
 ---------------------------   | ------------
 SoporteJeringa | 1
  SoporteMotor | 1
  SujetaManguera | 1
  PrensaJeringa60cc | 1
  CarroJeringa | 1
  EmboloCabezal | 1
  PortaJeringa60cc | 1
  TapaJeringa | 1
  TapaJeringaB | 1
  MangoHerramientaEmbolo | 1
  Embolo 60 cc | 1

## Herramientas

 ITEM |                  
 --------------------------- |  
 Tijera |
 Llave allen 2.5 mm |
 Llave allen 2 mm |
 Llave allen 1.5 mm |
 Broca 3 mm | 
 Llave francesa | 
 Taladro |
 Disco de corte |



