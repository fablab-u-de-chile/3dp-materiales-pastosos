# Bioprinter 

[![CC BY 4.0][cc-by-sa-shield]][cc-by-sa]

Impresora 3D de extrusión de biomateriales en formato pasta, que consiste en la integración de un cabezal hidráulico de 2 partes basado en jeringas en una impresora 3D estándar. La primera parte consiste en un actuador de jeringa controlado por un motor stepper NEMA17, el cual presiona agua a través de una manguera hacia la segunda parte del sistema, el cabezal, el que posee un émbolo que empuja a su vez una carga de material pastoso por la punta de la jeringa. Se espera que el flujo de trabajo se inicie con la Biomixer, donde se generan mezclas precisas que luego se vierten en los contenedores de la Bioimpresora 3D.

Este proyecto es parte de la iniciativa https://gitlab.com/fablab-u-de-chile/NBD liderada por el FabLab U. de Chile, financiado por el Ministerio de Culturas, Artes y Patrimonio de Chile, apoyado por la plataforma internacional Materiom, y desarrollado en colaboración con estudiantes e investigadores de la Universidad de Chile. 

<img src="/Bioprinter1.png" height="250"> <img src="/Bioprinter2.png" height="250">

Existe una tercera componente que corresponde a una herramienta accesoria para retirar fácilmente el émbolo del cabezal luego de agotar la carga de pasta.

Hasta ahora ha sido probado con arcilla, greda y silicona, en una impresora Makerbot Replicator 2 hackeada.

<img src="/img/cabezaliso.png" width="700">

<img src="/img/isojer.png" width="700">

## Atributos

- Compatible con impresoras 3D FDM corrientes y sus softwares. (1)
- Cambio rápido y limpio de cabezales.
- 50cc por carga de material.

(1) Para que esto sea posible es necesario poder calibrar los pasos/mm del extrusor para ajustar el sistema de tornillo. También el driver de la impresora debe ser capaz de alimentar el sistema. Estas condiciones las puede cumplir una impresora basada en Arduino y RAMPS, con Marlin como firmware.

[VIDEO](https://youtu.be/o1alwDbQAf8) funcionando.

## Cómo construirlo

- [LISTADO](parts.md) de partes, piezas y herramientas. 
- [Partes CAD](parts/).
- [ELECTRÓNICA Y SOFTWARE](elec.md).
- Instrucciones de ensamble.

## Trabajo futuro

Actualmente nos encontramos desarrollando un sistema que permita imprimir con materiales calientes, para abrirse al campo de los biomateriales que se cocinan. 

### Ponte en contacto

Puedes ver más de nuestra labor en:

- [Página Web](http://www.fablab.uchile.cl/)
- [Instagram](https://www.instagram.com/fablabudechile/?hl=es-la)
- [Youtube](https://www.youtube.com/channel/UC4pvq8aijaqn5aN02GiDwFA)

# Licencia
[![CC BY SA 4.0][cc-by-sa-image]][cc-by-sa]

Este trabajo esta publicado bajo la licencia [Creative Commons Attribution 4.0 International
License][cc-by-sa].

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY%20SA%204.0-lightgrey.svg
