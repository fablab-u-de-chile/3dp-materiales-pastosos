# Electrónica y software

 ITEM              | Cantidad
 ---------------------------   | ------------
Motor stepper NEMA17 | 1

Cualquier motor NEMA17 podrá actuar el sistema, pero mientras más torque tenga, mayor presión podrá lograr el extrusor, y por tanto, diversidad de materiales y velocidades de impresión. El contra es el aumento de corriente.

En caso de que se tenga una impresora DIY, es necesario calibrar la corriente máxima en el driver del stepper antes de hacer la conexión. Estos drivers suelen ser:

- DRV8825
- A4988

Puedes ver más información de este proceso [AQUÍ](https://www.pololu.com/product/2133).

Una forma para construir una nueva impresora o hackear una existente es mediante un Arduino MEGA y un RAMPS, junto con todos los accesorios que resten. Más información en este [LINK](https://reprap.org/wiki/Arduino_Mega_Pololu_Shield).

Una vez que se tiene la mecánica y la electrónica resueltas, sólo resta cargar el firmware, el cual puede ser [MARLIN](https://marlinfw.org/).

La idea es modificar el parámetro de pasos por milímetro del extrusor o eje E, de tal forma que coincida con el movimiento lineal del pistón en el actuador.

En el software de impresión, por ejemplo, [CURA](https://ultimaker.com/es/software/ultimaker-cura), es necesario ajustar el diámetro del filamento al tamaño del diámetro interno de la jeringa, y el diámetro de la boquilla respecto a la punta de la jeringa, para ajustar el flujo de material a la tasa correcta.






